npmcp
========================
Copy package and npm install to a new directory as it would be when installed
from NPM.

## Deprecated

**This repo and package have been deprecated. The same functionality now available in [ls-ignore](https://gitlab.com/bytesnz/ls-ignore).**